#include "jmd-inifile.hpp"
#include "jmd-utils.hpp"
#include <stdexcept>

Field absent_field;

const Field& Section::operator[](const std::string& varname) const
{
	std::string vn = tolower(varname);
	return data.count(vn)?data.at(vn):absent_field;
}

Sections::Iterator Sections::begin() { return Iterator(0,ini_ptr); }
Sections::Iterator Sections::end()   { return Iterator(ini_ptr->ordered_index.size(),ini_ptr); }

const Section& Sections::Iterator::operator*() const { return *(ini_ptr->ordered_index[m_ctr]); }
Section* Sections::Iterator::operator->() { return ini_ptr->ordered_index[m_ctr]; }

INIfile::INIfile(const std::string& contents) : sections(this)
{
	std::string line,section;
	
	// Empty name section for global parameters
	Section *new_sect = new(Section); //new_sect->set_val("","");
	index[""]=new_sect;
	ordered_index.push_back(new_sect);
	
	auto lines = split_string(contents,"\r\n"); 
	
	for (auto const& in_line: lines)
	{
		std::string line = trim(in_line," \t\r\n");
		if (line.empty() || (line.front() == '#') || (line.front() == ';')) continue;
		if ((line.front() == '[') && (line.back() == ']'))
		{
			// New section
			section = line;
			section = trim(section.substr(1,section.length()-2));
			std::string sec_name = section;
			section = tolower(section);
			
			if (index.count(section)==0)	// Create section if doesn't already exists
			{
				new_sect = new(Section);
				index.emplace(section,new_sect);
				ordered_index.push_back(new_sect);
			}
			else
				new_sect = index.at(section);	// already exists: add variables to it
			new_sect->name=sec_name;
			// Always setting name to be consistent with capitalization of variable names order
			// in duplicate variables, its the latter that gets kept.
		}
		else
		{
			// New field
			std::string key,val;
			size_t eq = line.find_first_of('=');
			if (eq == std::string::npos) key = line; 
			else
			{
				key = trim(line.substr(0,eq)); val = trim(line.substr(eq+1));
			}
			new_sect->set_val(tolower(key),val,key);
		}
	}
}

INIfile::~INIfile()
{
	for (auto const secptr: ordered_index)
		delete(secptr);
}

const Section& INIfile::operator[](int n) const
{
	return *ordered_index[n];
}

const Section& INIfile::operator[](const std::string& s) const
{
	//TODO: return non_present section if not found, instead of throwing(std::out_of_range)?
	return *index.at(tolower(s));
}

const Field& INIfile::operator()(const std::string& s) const
{
	auto parts = split_string(tolower(s),".");

	const std::string empty = "";

	const std::string& secname=(parts.size()==1)?empty:parts[0];
	const std::string& varname=(parts.size()==1)?parts[0]:parts[1];

	if (!index.count(secname)) return absent_field;
	if (!index.at(secname)->data.count(varname)) return absent_field;
	return index.at(secname)->data.at(varname);
}

