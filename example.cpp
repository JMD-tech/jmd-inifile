#include <iostream>
#include <string>

#include "jmd-utils.hpp"
#include "jmd-inifile.hpp"

using namespace std;

int main()
{
	string fname = "example.ini";
	cout << "Reading INI file " << fname << endl;
	string cont = file_get_contents(fname);
	//cout << "Contents:" << endl << cont << endl << endl;

	INIfile cfg(cont);

	cout << "VALUES SEARCH:" << endl;
	
	// Provided "first_section" is the first one (after global variables) in the order of the ini file, those 3 are equivalent:
	cout << "\t" << cfg[1]["variable1"] << endl;
	cout << "\t" << cfg["first_section"]["variable1"] << endl;
	cout << "\t" << cfg("first_section.variable1") << endl;

	// 3 ways to access the "global" variables: (unnamed "section" is always at index 0)
	cout << "\t" << cfg[0]["a_global_variable"] << endl;
	cout << "\t" << cfg[""]["a_global_variable"] << endl;	
	cout << "\t" << cfg("a_global_variable") << endl;

	cout << "\t" << "Accessing non-existant variable with () operator is safe:" << endl;
	cout << "\t\t" << "returns \"" << cfg("non_existant_section.non_existant_variable")
			<< "\" but no exception." << endl;	// Not with operator [] for the moment.
	cout << endl;

	cout << "ENUMERATING SECTIONS:" << endl;

	for (auto const& sec: cfg.sections)
	{
		cout << "Section name: \"" << sec.name << "\", contents:" << endl;
		for (auto const& var: sec)
			//cout << "\t" << var.first << " = " << var.second << endl;
			// or if you need the case-preserved variable names:
			cout << "\t" << var.second.name << " = " << var.second << endl;			
	}

	return 0;
}

