#ifndef __JMD_INIFILE_HPP__
#define __JMD_INIFILE_HPP__

#include <string>
#include <vector>
#include <map>
#include <cstddef>
#include <iterator>
#include <iostream>

class INIfile;

class Field
{
	public:
		std::string name, value;
		Field() {};
		Field(const std::string& in_name, const std::string& in_val):name(in_name),value(in_val) {};
		operator std::string() const { return value; };
		friend std::ostream & operator <<(std::ostream &out, const Field &obj) {
			return out << static_cast<std::string>(obj);
		}
};

extern Field absent_field;

class Section
{
	public:
		std::string name;
		const Field& operator[](const std::string& varname) const;
		std::map<std::string,Field>::const_iterator begin() const { return data.begin(); };
		std::map<std::string,Field>::const_iterator end() const { return data.end(); };
	private:
		std::map<std::string,Field> data;
		void set_val(const std::string& key, const std::string& val, const std::string& name) { data[key].value=val; data.at(key).name=name; };

	friend class INIfile;
};

//TODO: find some way to alias first => name and second => value

//TODO: nest into INIfile? => nope, can have its use alone (passing a Section reference to a subroutine)
class Sections
{
public:
	Sections(INIfile *in_ini_ptr):ini_ptr(in_ini_ptr) {};

	struct Iterator
	{
		using iterator_category = std::forward_iterator_tag;
		using difference_type   = std::ptrdiff_t;
		using value_type        = Section;
		using pointer           = Section*;
		using reference         = Section&;

		Iterator(int ctr, INIfile* in_ini_ptr) : m_ctr(ctr),ini_ptr(in_ini_ptr) {};

		const Section& operator*() const;
		pointer operator->();
		Iterator& operator++() { m_ctr++; return *this; };
		Iterator operator++(int) { Iterator tmp = *this; ++(*this); return tmp; };
		friend bool operator== (const Iterator& a, const Iterator& b) { return a.m_ctr == b.m_ctr; };
		friend bool operator!= (const Iterator& a, const Iterator& b) { return a.m_ctr != b.m_ctr; };

	private:
		INIfile *ini_ptr;
		int m_ctr;
	};

	Iterator begin();
	Iterator end();

private:
	INIfile *ini_ptr;
};

class INIfile
{
	public:
		INIfile(const std::string& contents);
		~INIfile();
		const Section& operator[](int n) const;
		const Section& operator[](const std::string& s) const;
		const Field& operator()(const std::string& s) const;
		Sections sections;
	private:
		std::vector<Section*> ordered_index;
		std::map<std::string,Section*> index;

	friend class Sections;
};

//TODO: either in INIfile or Sections: some size() and count() members,
// to allow checking existence of section, and oldschool int enumeration.
// (or some assignment/allocations dependent on sections numbers)

#endif
