
CXXFLAGS+= -std=c++11 -ljmd-utils
#TODO: have this by default, but allow overriding
PREFIX=/usr/local

default: all

all: staticlib example

clean:
	rm -f *.o *.a *.so example

staticlib: jmd-inifile.o
	ar -rcs libjmd-inifile.a jmd-inifile.o

example: staticlib
	$(CXX) example.cpp libjmd-inifile.a -o example $(CXXFLAGS) -lstdc++fs

install: staticlib
	install -d $(DESTDIR)$(PREFIX)/lib/
	install -m 644 libjmd-inifile.a $(DESTDIR)$(PREFIX)/lib/
	install -d $(DESTDIR)$(PREFIX)/include/
	install -m 644 jmd-inifile.hpp $(DESTDIR)$(PREFIX)/include/


