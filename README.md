## JMD-inifile

A very lightweight and simple usage C++ INI-file parser.

### Usage:

You can see [example.cpp](https://gitlab.com/JMD-tech/jmd-inifile/-/blob/master/example.cpp) for the whole functions, 
including a way to enumerate sections by the order of the file, and more details, but for the basic usage
that covers >90% use cases it boils down to:

	#include <iostream> 
	#include "jmd-inifile.hpp"
	#include "jmd-utils.hpp"
	
	using std::cout; using std::endl;
	
	int main()
	{
	    INIfile cfg(file_get_contents("example.ini"));
	
	    // Standalone variable:
	    cout << cfg("a_global_variable") << endl;
	
	    // Variable in section [First_Section]:
	    cout << cfg("first_section.variable1") << endl;
	    // or:
	    cout << cfg["first_section"]["variable1"] << endl;
	
	    return 0;
	}

note1: The operator () version returns empty-string on non existant section or variable (no exception).  
The [] version does throw std::out_of_range exception on non existant. The latter might change.  

note2: As INI file are meant to be case insensitive, the indexes of sections and variables names are internally stored 
in lowercase. The () and [] operators already do a tolower() conversion so it doesn't matter how you search them.
If one would really need to read how they were capitalized in the file, one can use the .name member of the sections and fields objects.
In case of duplicate sections or variable names, it's the latter capitalization that gets kept in .name member.

#### Linking in your program:

	g++ myprogram.cpp -ljmd-inifile -ljmd-utils

### Installation instructions:

First install [JMD-utils](https://gitlab.com/JMD-tech/jmd-utils):  

	git clone https://gitlab.com/JMD-tech/jmd-utils  
	cd jmd-utils  
	make install  
	cd ..  

Then install JMD-inifile itself:

	git clone https://gitlab.com/JMD-tech/jmd-inifile  
	cd jmd-inifile  
	make install  

To compile and test the example program:

	make example
	./example


### Design notes:

Some of the notes about requirements, interface and implementation written before starting coding this lib:  

Requirements: (already implemented in current version)  
- My own small-libraries generic policies: no dependencies on huge and/or external libraries, allow static-linking, pure easily readable Makefile without autotools/autoconf/automake/automess or exotic build system
- C++ version: Obviously omitting plain C (noone sane enough still uses this for other things than kernel, drivers, plugins or (extremely constrained) microcontrollers programming), yet keeping the compiler+environment requirement very low (C++11 and higher)
- Allow arbitrary section/fields names / and iteration by order in file, as well as associative array access
- Allow case insensitive (but case-preserving) access to sections/fields names, without adding dependency hell on intl etc libs (anyone sensible enough won't use accented characters in variable or sections names of an INI or .conf file - probably not "standard" anyway, and never seen "in the wild")
- Obviously, don't fail on LF/CRLF nonsense (and handle anyone gracefully on any platform)

Planned or envisioned future developments:  
- Implicit conversions to int and bool values (maybe float too?)
- Optionnal checking of mandatory fields, and setting default values for optionnal ones
- Range for numerical values
- enable specification/override of INI parameters by command line arguments
- maybe along with those above, some automatic --help output generation
- microcontroller environment version (might drop some things like auto tolower() in index search)

Will never implement: (in this library)  
- Any kind of parsing of the contents of the data values, besides checking correctness (type and range).
Things like expressions, environment variable substitutions etc, can be further implemented into the application code itself,
maybe in a secondary library, but won't be integrated into this library, as to keep simplicity for the 99% use case we don't need those.
- Same thing for complex data types, arrays/object structs etc. 
(If you need those, JSON is more suited than INI file format, or for a single use nothing forbids you from storing a comma(or other) delimited string in an INI parameter to be parsed later)
- Ditto for any kind of localization / character set handling: The contents of the data values may be into whatever one you want to interpret later, 
but the sections and variables names are A-Za-Z0-9_(* ), so to avoid bringing dependencies on localization libs we use a simplistic tolower/upper conversion
for case insensitive operations.
(*) not enforced by this library, nothing prevents you from reading a variable with a name with accents, except good practice and common sense 
(and the generally accepted "standard" for INI file syntax).


